package tallerlambdas;

import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import tallerlambdas.domain.Alumno;
import tallerlambdas.service.AlumnoService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TallerLambdasApplication.class)
public class StreamsTest {

    @Autowired
    private AlumnoService alumnoService;

    @Test
    public void buscarAlumnosMayoresA50_conAlumnosExistentes_devuelveListaEdades() {
        List<Alumno> alumnos = alumnoService.obtenerAlumnosMayoresAunaEdad(50);

        assertEquals(4, alumnos.size());
    }

    @Test
    public void sonTodosMayoresA2anios_conAlumnosExistentes_devuelveTrue() {
        assertTrue(alumnoService.sonTodosMayoresA2anios());
    }
}
