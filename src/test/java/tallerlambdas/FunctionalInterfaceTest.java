package tallerlambdas;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import tallerlambdas.domain.Nota;
import tallerlambdas.service.NotaService;
import tallerlambdas.utils.CalculadoraDeNotas;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TallerLambdasApplication.class)
public class FunctionalInterfaceTest {

    @Autowired
    private NotaService notaService;

    private Nota nota1, nota2, nota3;
    private CalculadoraDeNotas calculadoraDeNotas;

    @Before
    public void setUp() {
        nota1 = notaService.obtenerNotaPorId(1l);
        nota2 = notaService.obtenerNotaPorId(2l);
        nota3 = notaService.obtenerNotaPorId(3l);
    }

    @Test
    public void ejecutarOperacionEntreTresNotas_sumar_devuelve9() {

        //calculadoraDeNotas = 
        assertEquals(new Double(9), notaService.ejecutarOperacionEntreDosNotas(nota1, nota2, nota3, calculadoraDeNotas));

    }

    @Test
    public void ejecutarOperacionEntreTresNotas_promedio_devuelve3() {

        //calculadoraDeNotas = 
        assertEquals(new Double(3), notaService.ejecutarOperacionEntreDosNotas(nota1, nota2, nota3, calculadoraDeNotas));

    }

}
