package tallerlambdas.utils;

import tallerlambdas.domain.Nota;

public interface CalculadoraDeNotas {

    Double calcular(Nota nota1, Nota nota2, Nota nota3);

}
