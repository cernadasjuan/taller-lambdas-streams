package tallerlambdas.service;

import java.util.List;
import tallerlambdas.domain.Alumno;

public interface AlumnoService {

    List<Alumno> obtenerTodosLosAlumnos();

    Alumno obtenerAlumnoPorId(Long id);

    boolean sonTodosMayoresA2anios();

    List<Alumno> obtenerAlumnosMayoresAunaEdad(int edad);

}
