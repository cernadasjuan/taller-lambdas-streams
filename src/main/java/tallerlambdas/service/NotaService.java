package tallerlambdas.service;

import java.util.List;
import tallerlambdas.domain.Nota;
import tallerlambdas.utils.CalculadoraDeNotas;

public interface NotaService {

    List<Nota> obtenerTodasLasNotas();

    Nota obtenerNotaPorId(Long id);

    Double ejecutarOperacionEntreDosNotas(Nota nota1, Nota nota2, Nota nota3, CalculadoraDeNotas calculadoraDeNotas);

}
