package tallerlambdas.serviceImpl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tallerlambdas.domain.Alumno;
import tallerlambdas.repository.AlumnoRepository;
import tallerlambdas.service.AlumnoService;

@Service
public class AlumnoServiceImpl implements AlumnoService {

    @Autowired
    private AlumnoRepository alumnoRepository;

    @Override
    public List<Alumno> obtenerTodosLosAlumnos() {
        return alumnoRepository.findAll();
    }

    @Override
    public Alumno obtenerAlumnoPorId(Long id) {
        return alumnoRepository.findOne(id);
    }

    @Override
    public List<Alumno> obtenerAlumnosMayoresAunaEdad(int edad) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean sonTodosMayoresA2anios() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
