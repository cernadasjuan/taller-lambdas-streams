package tallerlambdas.serviceImpl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tallerlambdas.domain.Nota;
import tallerlambdas.repository.NotaRepository;
import tallerlambdas.service.NotaService;
import tallerlambdas.utils.CalculadoraDeNotas;

@Service
public class NotaServiceImpl implements NotaService {

    @Autowired
    private NotaRepository notaRepository;

    @Override
    public List<Nota> obtenerTodasLasNotas() {
        return notaRepository.findAll();
    }

    @Override
    public Nota obtenerNotaPorId(Long id) {
        return notaRepository.findOne(id);
    }

    @Override
    public Double ejecutarOperacionEntreDosNotas(Nota nota1, Nota nota2, Nota nota3, CalculadoraDeNotas calculadoraDeNotas) {
        return calculadoraDeNotas.calcular(nota1, nota2, nota3);
    }

}
