package tallerlambdas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tallerlambdas.domain.Alumno;

public interface AlumnoRepository extends JpaRepository<Alumno, Long> {

}
