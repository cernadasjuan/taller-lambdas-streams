package tallerlambdas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tallerlambdas.domain.Nota;

public interface NotaRepository extends JpaRepository<Nota, Long> {

}
