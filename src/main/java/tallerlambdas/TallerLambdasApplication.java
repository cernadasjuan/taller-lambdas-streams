package tallerlambdas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TallerLambdasApplication {

    public static void main(String[] args) {
        SpringApplication.run(TallerLambdasApplication.class, args);
    }
}
